#!/usr/bin/env python

import time
import Adafruit_ADS1x15

import time

import Adafruit_GPIO.SPI as SPI
import Adafruit_SSD1306

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

import subprocess

# screen configuration
RST = None     # on the PiOLED this pin isnt used
# Note the following are only used with SPI:
DC = 23
SPI_PORT = 0
SPI_DEVICE = 0

#adc = Adafruit_ADS1x15.ADS1015()
#adc = Adafruit_ADS1x15.ADS1015(0x49)
adc = Adafruit_ADS1x15.ADS1115(0x49)
GAIN = 1
disp = Adafruit_SSD1306.SSD1306_128_32(rst=RST)

disp.begin()
disp.clear()
disp.display()

width = disp.width
height = disp.height
image = Image.new('1', (width, height))

draw = ImageDraw.Draw(image)
draw.rectangle((0,0,width,height), outline=0, fill=0)
padding = -2
top = padding
bottom = height-padding
x = 0
font = ImageFont.load_default()

#print('Reading ADS1x15 values, press Ctrl-C to quit...')
#print('| {0:>14} | {1:>14} | {2:>14} | {3:>14} |'.format(*range(4)))
#print('-' * 65)
while True:
    values = [0]*4
    for i in range(4):
        values[i] = adc.read_adc(i, gain=GAIN) #/327.68
    #print(values[0], '\r',)
    draw.rectangle((0,0,width,height), outline=0, fill=0)
    cmd = "hostname -I | cut -d\' \' -f1"
    IP = subprocess.check_output(cmd, shell = True )
    draw.text((x, top),       "IP: " + str(IP),  font=font, fill=255)
    reading = adc.read_adc(0, gain=1)
    print(reading)
    draw.text((x, top+8),     str(reading), font=font, fill=255)
    disp.image(image)
    disp.display()
    #print('| {0:>9} | {1:>9} | {2:>9} | {3:>9} |'.format(*values))
    time.sleep(1)
