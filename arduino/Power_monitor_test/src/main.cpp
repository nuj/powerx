#include <Arduino.h>

const unsigned int numReadings = 400; //samples to calculate Vrms.
 
int readingsVClamp[numReadings];    // samples of the sensor SCT-013-000
int readingsGND[numReadings];      // samples of the <span id="result_box" class="" lang="en"><span class="hps">virtual</span> <span class="hps alt-edited">ground</span></span>
float SumSqGND = 0;            
float SumSqVClamp = 0;
float total = 0; 

float currentPrimaryPeak;
float currentSecondaryPeak;
int currentRange = 100;
float currentSensed[numReadings];

//const float calibration = 0.44921875 * 3;
//const float calibration = 1.0F;
const float calibration = 1.4;
 
int PinVClamp = GPIO_NUM_2;    // Sensor SCT-013-000
int PinVirtGND = GPIO_NUM_15;   // <span id="result_box" class="" lang="en"><span class="hps">Virtual</span> <span class="hps alt-edited">ground</span></span>
 
void setup() {
  Serial.begin(9600);
  //Serial.println("Hello World!");
  // initialize all the readings to 0:
  for (int thisReading = 0; thisReading < numReadings; thisReading++) {
    readingsVClamp[thisReading] = 0;
    readingsGND[thisReading] = 0;
  }
}
 
void loop() {
  unsigned int i=0;
  SumSqGND = 0;
  SumSqVClamp = 0;
  total = 0; 
   
  for (unsigned int i=0; i<numReadings; i++)
  {
    readingsVClamp[i] = analogRead(PinVClamp) - analogRead(PinVirtGND);
    delay(1); // 
  } 



/* burden resistor (ohms) = (3.3 / 2) / currentSecondaryPeak = 

currentPrimaryPeak = currentRange * sqrt(2); // primary peak-current
currentSecondaryPeak = currentPrimaryPeak / 2000; // secondary peak-current

(3.3 / 2) / ((0.7 * sqrt(2)) / 2000) where 0.7 is current measuring range in A, 3.3 is the esp32 input voltage = burden resistor value in Ohms
currently Rburden is 3.3k Ohms which means that the current range is set to 0.(70)A
for 100A current range a 23 ohm resitor is needed... probably
*/


/*for(int i = 0; i < numReadings; i++)
{
   Serial.print(readingsVClamp[i]);
   Serial.print(" ");
}
Serial.println();
*/
  //Calculate Vrms
  for (unsigned int i=0; i<numReadings; i++)
  {
    SumSqVClamp = SumSqVClamp + sq((float)readingsVClamp[i]);
 
  }

  total = sqrt(SumSqVClamp/numReadings);
  //total= (total*(float)2/3); // Rburden=3300 ohms, LBS=  V (3.3/1024)
  total = (total - 10) * calibration;   // Transformer of 2000 laps (SCT-013-000).
                                   // 3.3*230*2000/(3300*1024)= 115/256 (aprox)
   
  Serial.println(total);
  delay(1500);  
}