#!/usr/bin/env python

import time
import Adafruit_ADS1x15

adc = Adafruit_ADS1x15.ADS1115(0x49)
GAIN = 1

data = ""
for i in range(50):
    reading = adc.read_adc(0, gain=1)
    data = data + '\n' + str(reading)
    time.sleep(0.0001)
print(data)
