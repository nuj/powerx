#!/usr/bin/env python

import paho.mqtt.client as mqtt
import time

def on_connect(client, userdata, flags, rc):
    if rc==0:
        client.connected_flag=True
        print("connected OK")
    else:
        print("Bad connection Returned code=",rc)

mqtt.Client.connected_flag=False
broker="mqtt.coventry.ac.uk"
client = mqtt.Client("python1")
client.username_pw_set("powerx", "nbCXeTrPxX")
client.tls_set("./mqtt.crt") 
client.on_connect=on_connect
client.loop_start()
print("Connecting to broker ",broker)
client.connect(broker, 8883)
while not client.connected_flag:
    print("In wait loop")
    time.sleep(1)
print("in Main Loop")
client.publish("powerx/hello", "Hello World", qos=0, retain=False)
client.loop_stop()
client.disconnect()
